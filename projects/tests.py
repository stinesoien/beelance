from django.core import mail
import string, random
from django.test import TestCase
from django.http import request
from projects.views import project_view, send_mail, get_user_task_permissions
from .models import Project, ProjectCategory, TaskOffer, Task, Team
from user.models import Profile
from django.contrib.auth.models import User
from unittest.mock import patch

from .forms import TaskOfferForm

# Create your tests here.
class TaskOfferTests(TestCase):
    def test_task_offer_empty(self):
        form_data = {
            'title': '',
            'description': '',
            'price': ''
        }
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())
    def test_task_offer_min(self):
        form_data = {
            'title': 'a',
            'description': 'a',
            'price': 0
        }
        form = TaskOfferForm(data=form_data)
        self.assertTrue(form.is_valid())
    def test_task_offer_decimal_price(self):
        form_data = {
            'title': 'a',
            'description': 'a',
            'price': 0.1
        }
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())
    def test_task_offer_umin_price(self):
        form_data = {
            'title': 'a',
            'description': 'a',
            'price': -1
        }
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())
    def test_task_offer_max_title(self):
        form_data = {
            'title': ''.join(random.choice(string.ascii_lowercase) for _ in range(200)),
            'description': 'a',
            'price': 0
        }
        form = TaskOfferForm(data=form_data)
        self.assertTrue(form.is_valid())
    def test_task_offer_omax_title(self):
        form_data = {
            'title': ''.join(random.choice(string.ascii_lowercase) for _ in range(201)),
            'description': 'a',
            'price': 0
        }
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())
    def test_task_offer_max_desc(self):
        form_data = {
            'title': 'a',
            'description': ''.join(random.choice(string.ascii_lowercase) for _ in range(500)),
            'price': 0
        }
        form = TaskOfferForm(data=form_data)
        self.assertTrue(form.is_valid())
    def test_task_offer_omax_desc(self):
        form_data = {
            'title': 'a',
            'description': ''.join(random.choice(string.ascii_lowercase) for _ in range(501)),
            'price': 0
        }
        form = TaskOfferForm(data=form_data)
        self.assertFalse(form.is_valid())

class ProjectVeiwTest(TestCase):
    def setUp(self):
        ProjectCategory.objects.create(name="Category 1", active=True)
        User.objects.create(username="testing", email="My_Test_Mail")
        Project.objects.create(user=Profile.objects.get(pk=1),
                               category_id=ProjectCategory.objects.get(name="Category 1").id, title="my Projectos")
        Task.objects.create(project_id=1)
        TaskOffer.objects.create(task_id=1, offerer_id=1)

    def test_project_view_accepted(self):
        myReq = request.HttpRequest()
        myReq.user = User.objects.get(username="testing")
        myReq.method = "POST"
        myReq.POST["offer_response"] = "offer_response_example"
        myReq.POST["taskofferid"] = 1
        myReq.POST["feedback"] = "Yes"
        myReq.POST["status"] = 'a'
        mock_messages = patch('projects.views.django.shortcuts').start()
        messages = []

        def test_message_input(request, place, data):
            messages.append([request, place, data])

        mock_messages.render = test_message_input
        project_view(myReq, 1)
        data = messages[0][2]
        myRequest = messages[0][0]
        self.assertEqual(Project.objects.get(id=1), data["project"])
        self.assertEqual(Task.objects.get(id=1), data["tasks"].get(id=1))
        self.assertTrue(myRequest.POST["offer_response"] == myReq.POST["offer_response"])
        self.assertTrue(myRequest.POST["taskofferid"] == myReq.POST["taskofferid"])
        self.assertTrue(myRequest.POST["feedback"] == myReq.POST["feedback"])
        self.assertTrue(myRequest.POST["status"] == myReq.POST["status"])
        self.assertTrue(myRequest.user == myReq.user)
        self.assertEqual(len(mail.outbox), 1)

    def test_project_view_declined(self):
        myReq = request.HttpRequest()
        myReq.user = User.objects.get(username="testing")
        myReq.method = "POST"
        myReq.POST["offer_response"] = "offer_response_example"
        myReq.POST["taskofferid"] = 1
        myReq.POST["feedback"] = "Yes"
        myReq.POST["status"] = 'd'
        mock_messages = patch('projects.views.django.shortcuts').start()
        messages = []

        def test_message_input(request, place, data):
            messages.append([request, place, data])

        mock_messages.render = test_message_input
        project_view(myReq, 1)
        data = messages[0][2]
        myRequest = messages[0][0]
        self.assertEqual(Project.objects.get(id=1), data["project"])
        self.assertEqual(Task.objects.get(id=1), data["tasks"].get(id = 1))
        self.assertTrue(myRequest.POST["offer_response"] == myReq.POST["offer_response"])
        self.assertTrue(myRequest.POST["taskofferid"] == myReq.POST["taskofferid"])
        self.assertTrue(myRequest.POST["feedback"] == myReq.POST["feedback"])
        self.assertTrue(myRequest.POST["status"] == myReq.POST["status"])
        self.assertTrue(myRequest.user == myReq.user)
        self.assertEqual(len(mail.outbox), 0)

    def test_project_view_pending(self):
        myReq = request.HttpRequest()
        myReq.user = User.objects.get(username="testing")
        myReq.method = "POST"
        myReq.POST["offer_response"] = "offer_response_example"
        myReq.POST["taskofferid"] = 1
        myReq.POST["feedback"] = "Yes"
        myReq.POST["status"] = 'p'
        mock_messages = patch('projects.views.django.shortcuts').start()
        messages = []

        def test_message_input(request, place, data):
            messages.append([request, place, data])

        mock_messages.render = test_message_input
        project_view(myReq, 1)
        data = messages[0][2]
        myRequest = messages[0][0]
        self.assertEqual(Project.objects.get(id=1), data["project"])
        self.assertEqual(Task.objects.get(id=1), data["tasks"].get(id = 1))
        self.assertTrue(myRequest.POST["offer_response"] == myReq.POST["offer_response"])
        self.assertTrue(myRequest.POST["taskofferid"] == myReq.POST["taskofferid"])
        self.assertTrue(myRequest.POST["feedback"] == myReq.POST["feedback"])
        self.assertTrue(myRequest.POST["status"] == myReq.POST["status"])
        self.assertTrue(myRequest.user == myReq.user)
        self.assertEqual(len(mail.outbox), 0)

    def test_project_view_status_changed(self):
        myReq = request.HttpRequest()
        myReq.user = User.objects.get(username="testing")
        myReq.method = "POST"
        myReq.POST["status_change"] = "offer_response_example"
        myReq.POST["taskofferid"] = 1
        myReq.POST["feedback"] = "Yes"
        myReq.POST["status"] = 'i'
        mock_messages = patch('projects.views.django.shortcuts').start()
        messages = []

        def test_message_input(request, place, data):
            messages.append([request, place, data])

        mock_messages.render = test_message_input
        project_view(myReq, 1)
        data = messages[0][2]
        myRequest = messages[0][0]
        self.assertEqual(Project.objects.get(id=1), data["project"])
        self.assertEqual(Task.objects.get(id=1), data["tasks"].get(id = 1))
        self.assertTrue(myRequest.POST["taskofferid"] == myReq.POST["taskofferid"])
        self.assertTrue(myRequest.POST["feedback"] == myReq.POST["feedback"])
        self.assertTrue(myRequest.POST["status"] == myReq.POST["status"])
        self.assertTrue(myRequest.user == myReq.user)
        self.assertEqual(len(mail.outbox), 0)

    def test_send_mail(self):
        myRequest = request.HttpRequest()
        subject = "Subject"
        message = "Message"
        from_email = "From_email"
        to_email = "To_email"
        send_mail(request=myRequest, subject=subject, message=message, from_email=from_email, to_email=[to_email])
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, subject)
        self.assertEqual(mail.outbox[0].body, message)

    def test_send_mail_exception(self):
        myRequest = request.HttpRequest()
        subject = "Subject"
        message = "Message"
        from_email = "From_email"
        to_email = "To_email"
        errorString = 'Sending of email to ' + to_email + ' failed: "to" argument must be a list or tuple'
        mock_messages = patch('projects.views.messages').start()
        messages = []

        def test_message_input(request, myStr):
            messages.append([request, myStr])

        mock_messages.success = test_message_input
        send_mail(request=myRequest, subject=subject, message=message, from_email=from_email, to_email=to_email)
        self.assertTrue(messages[0][0] == myRequest and errorString == messages[0][1])
        self.assertEqual(len(mail.outbox), 0)


class getUserTaskPermissionsTest(TestCase):
    def setUp(self):
        ProjectCategory.objects.create(name="Category 1", active=True)
        User.objects.create(username="owner_of_project", email="My_Test_Mail")
        User.objects.create(username="offerer_for_task", email="My_Test_Mail")
        User.objects.create(username="random_user", email="My_Test_Mail")
        Project.objects.create(user=Profile.objects.get(pk=1),
                               category_id=ProjectCategory.objects.get(name="Category 1").id, title="my Projectos")
        Task.objects.create(project_id=1)
        TaskOffer.objects.create(task_id=1, offerer_id=2)
        Team.objects.create(name="My test team", write=True, task=Task.objects.get(id=1))

    def test_is_owner(self):
        permissions = get_user_task_permissions(user=User.objects.get(id=1), task=Task.objects.get(id=1))
        self.assertTrue(all(permissions[i] for i in permissions))

    def test_accepted_offerer(self):
        TaskOffer.objects.filter(id=1).update(status="a")
        permissions = get_user_task_permissions(User.objects.get(id=2), Task.objects.get(id=1))
        self.assertTrue(
            all((permissions[i] and i != "owner") or (i == "owner" and not permissions[i]) for i in permissions))

    def test_no_permissions(self):  # Verify that no user gets automatic permissions
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(any(permissions[i] for i in permissions))

    def test_read_permission(self):
        Task.objects.get(id=1).read.add(Profile.objects.get(id=3))
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(
            any((permissions[i] and i != "read") or (i == "read" and not permissions[i]) for i in permissions))

    def test_write_permission(self):
        Task.objects.get(id=1).write.add(Profile.objects.get(id=3))
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(
            any((permissions[i] and i != "write") or (i == "write" and not permissions[i]) for i in permissions))

    def test_modify_permission(self):
        Task.objects.get(id=1).modify.add(Profile.objects.get(id=3))
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(
            any((permissions[i] and i != "modify") or (i == "modify" and not permissions[i]) for i in permissions))

    def test_upload_permission(self):  # Upload will also give view_task permission
        Team.objects.get(id=1).members.add(Profile.objects.get(id=3))
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(
            any((permissions[i] and i not in ("view_task", "upload")) or (
                    i in ("view_task", "upload") and not permissions[i]) for i in permissions))

    def test_view_permission_by_team(self):
        Team.objects.filter(id=1).update(write=False)
        Team.objects.get(id=1).members.add(Profile.objects.get(id=3))
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(
            any((permissions[i] and i not in ("view_task")) or (
                    i in ("view_task") and not permissions[i]) for i in permissions))

    def test_view_permission_by_request(self):
        Team.objects.get(id=1).requests.add(Profile.objects.get(id=3))
        permissions = get_user_task_permissions(User.objects.get(id=3), Task.objects.get(id=1))
        self.assertFalse(
            any((permissions[i] and i not in ("view_task")) or (
                    i in ("view_task") and not permissions[i]) for i in permissions))
