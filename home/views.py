from django.shortcuts import render, redirect

from projects.models import Project


def home(request):
    if request.user.is_authenticated:
        user = request.user
        user_projects = Project.objects.filter(user=user.profile)
        customer_projects = list(Project.objects.filter(participants__id=user.id))
        for team in user.profile.teams.all():
            customer_projects.append(team.task.project)
        customer_projects = sorted(customer_projects, key=lambda p: p.id)
        given_offers_projects = Project.objects.filter(pk__in=get_given_offer_projects(user), status=Project.OPEN).distinct()
        participation_requests = Project.objects.filter(tasks__teams__requests__id=user.id).distinct()

        return render(
            request,
            'index.html',
            {
                'user_projects': user_projects,
                'customer_projects': customer_projects,
                'given_offers_projects': given_offers_projects,
                'participation_requests': participation_requests,
            })
    else:
        return redirect('projects')


def get_given_offer_projects(user):
    project_ids = set()

    for taskoffer in user.profile.taskoffer_set.all():
        project_ids.add(taskoffer.task.project.id)

    return project_ids
