import string, random
from django.test import TestCase
from .forms import SignUpForm
from projects.models import ProjectCategory

# Create your tests here.
class SignUpTests(TestCase):
    def setUp(self):
        ProjectCategory(name="Test", active=True).save()
    def test_signup_empty(self):
        form_data = {
            'username': '',
            'first_name': '',
            'last_name': '',
            'company': '',
            'phone_number': '',
            'street_address': '',
            'city': '',
            'state': '',
            'postal_code': '',
            'country': '',
            'email': '',
            'email_confirmation': '',
            'password1': '',
            'password2': '',
            'categories': []
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_min(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_username_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': ''.join(random.choice(string.ascii_lowercase) for _ in range(150)),
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_username_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': ''.join(random.choice(string.ascii_lowercase) for _ in range(151)),
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_username_invalidchar(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': ''.join(random.choice(string.ascii_lowercase) for _ in range(100)) + '^',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_username_specialchars(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': ''.join(random.choice(string.ascii_lowercase) for _ in range(100)) + '@.+-_',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_firstname_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': ''.join(random.choice(string.ascii_lowercase) for _ in range(30)),
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_firstname_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': ''.join(random.choice(string.ascii_lowercase) for _ in range(31)),
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_lastname_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': ''.join(random.choice(string.ascii_lowercase) for _ in range(30)),
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_lastname_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': ''.join(random.choice(string.ascii_lowercase) for _ in range(31)),
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_company_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': ''.join(random.choice(string.ascii_lowercase) for _ in range(30)),
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_company_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': ''.join(random.choice(string.ascii_lowercase) for _ in range(31)),
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_phone_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_phone_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': ''.join(random.choice(string.ascii_lowercase) for _ in range(51)),
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_address_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_address_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': ''.join(random.choice(string.ascii_lowercase) for _ in range(51)),
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_city_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_city_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': ''.join(random.choice(string.ascii_lowercase) for _ in range(51)),
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_state_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_state_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': ''.join(random.choice(string.ascii_lowercase) for _ in range(51)),
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_postalcode_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_postalcode_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': ''.join(random.choice(string.ascii_lowercase) for _ in range(51)),
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_country_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_country_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': ''.join(random.choice(string.ascii_lowercase) for _ in range(51)),
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_email_max(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        email = ''.join(random.choice(string.ascii_lowercase) for _ in range(244)) + '@test.test'
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': ''.join(random.choice(string.ascii_lowercase) for _ in range(50)),
            'email': email,
            'email_confirmation': email,
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_email_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        email = ''.join(random.choice(string.ascii_lowercase) for _ in range(245)) + '@test.test'
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': email,
            'email_confirmation': email,
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_email_nomatch(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': ''.join(random.choice(string.ascii_lowercase) for _ in range(244)) + '@test.test',
            'email_confirmation': ''.join(random.choice(string.ascii_lowercase) for _ in range(244)) + '@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_password_min(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_password_umin(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(7))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_password_large(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(10000))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertTrue(form.is_valid(), msg=form.errors.as_text())
    def test_signup_password_nomatch(self):
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': ''.join(random.choice(string.ascii_lowercase) for _ in range(100)),
            'password2': ''.join(random.choice(string.ascii_lowercase) for _ in range(100)),
            'categories': [category.pk]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_categories_umin(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': []
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_categories_omax(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [category.pk, category.pk + 1]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())
    def test_signup_categories_invalid(self):
        password = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
        category = ProjectCategory.objects.get(name="Test")
        form_data = {
            'username': 'a',
            'first_name': 'a',
            'last_name': 'a',
            'company': '',
            'phone_number': 'a',
            'street_address': 'a',
            'city': 'a',
            'state': 'a',
            'postal_code': 'a',
            'country': 'a',
            'email': 'test@test.test',
            'email_confirmation': 'test@test.test',
            'password1': password,
            'password2': password,
            'categories': [-1]
        }
        form = SignUpForm(data=form_data)
        self.assertFalse(form.is_valid(), msg=form.errors.as_text())